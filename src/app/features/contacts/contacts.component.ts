import { Component, OnInit } from '@angular/core';

interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[]
}
interface City {
  id: number;
  name: string
}

@Component({
  selector: 'app-contacts',
  template: `
    <app-tab-bar
      [items]="countries"
      [active]="selectedCountry"
      (tabClick)="selectCountryHandler($event)"
    ></app-tab-bar>

    <app-tab-bar
      *ngIf="selectedCountry"
      [items]="selectedCountry?.cities"
      [active]="selectedCity"
      (tabClick)="selectCityHandler($event)"
    ></app-tab-bar>
    
    <app-card
      *ngIf="selectedCity"
      [title]="selectedCity?.name" 
      icon="link"
      (iconClick)="openURL('https://it.wikipedia.org/wiki/' + selectedCity.name)"
    >
      <app-mapquest [city]="selectedCity.name" [zoom]="10"></app-mapquest>
    </app-card>
    

  `,
})
export class ContactsComponent  {
  countries: Country[] = []
  selectedCountry: Country | undefined;
  selectedCity: City | undefined;

  constructor() {
    setTimeout(() => {
      this.countries = [
        { id: 1, name: 'Italy', desc: 'bla bla 1', cities: [ { id: 1, name: 'Rome'}, { id: 2, name: 'MIlano'}]},
        { id: 2, name: 'Germany', desc: 'bla bla 2', cities: [{ id: 4, name: 'Berlin'}]},
        { id: 3, name: 'Spain', desc: 'bla bla 3', cities: [ { id: 5, name: 'Madrid'}, { id: 6, name: 'Barcellona'}, { id: 7, name: 'Valencia'}]},
      ]
     this.selectCountryHandler( this.countries[0] )
    }, 300)
  }
  visible = false;

  selectCountryHandler(c: Country) {
    this.selectedCountry = c;
    this.selectedCity = this.selectedCountry.cities[0]
  }

  selectCityHandler(city: City) {
    this.selectedCity  = city
  }



  openURL(url: string) {
    window.open(url)
  }

  alert(msg: string) {
    window.alert(msg)
  }


}
