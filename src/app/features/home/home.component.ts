import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-home',
  template: `
    <p class="bg">
      home works! {{themeService.theme}}
      <button (click)="themeService.theme = 'dark'">set dark</button>
      <button (click)="themeService.theme = 'light'">set light</button>
    </p>
  `,
})
export class HomeComponent {
  constructor(public themeService: ThemeService) {
    console.log('home constructor', themeService.theme)
  }


}
