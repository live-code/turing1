import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list',
  template: `
    <app-users-list-item
      *ngFor="let user of users"
      [user]="user"
      [active]="active"
      (selectUser)="selectUser.emit($event)"
      (deleteUser)="deleteUser.emit($event)"
    ></app-users-list-item>
    
  `,
})
export class UsersListComponent {
  @Input() users: User[] = [];
  @Input() active: User | null = null;
  @Output() deleteUser = new EventEmitter<User>()
  @Output() selectUser = new EventEmitter<User>()

}
