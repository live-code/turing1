import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list-item',
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'bg-warning': user.id === active?.id}"
      (click)="selectUser.emit(user)"
    >
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <div>
            <app-icon-gender [gender]="user.gender"></app-icon-gender>
            {{user.name}}
          </div>

          <div *ngIf="isOpen">
            <app-mapquest [zoom]="5" [city]="user.city"></app-mapquest>
            <div>{{user.city}}</div>
          </div>
        </div>
        <div>
          <i class="fa fa-info-circle me-3" [routerLink]="'/users/' + user.id" ></i>
          <i class="fa fa-trash me-3" (click)="deleteUserHandler(user, $event)"></i>
          <i class="fa fa-map-marker" (click)="isOpen = !isOpen"></i>
        </div>
      </div>
    </li>
  `,
})
export class UsersListItemComponent  {
  @Input() user!: User;
  @Input() active: User | null = null;
  @Output() deleteUser = new EventEmitter<User>()
  @Output() selectUser = new EventEmitter<User>()
  isOpen = false;

  deleteUserHandler(userToDelete: User, event: MouseEvent) {
    event.stopPropagation();
    this.deleteUser.emit(userToDelete)
  }

}
