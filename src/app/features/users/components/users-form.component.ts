import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <div *ngIf="inputName.errors?.['required']">Campo Obbligatorio</div>
      <div *ngIf="inputName.errors?.['minlength']">
        Campo troppo corto. Mancano
        {{inputName.errors?.['minlength']['requiredLength'] - inputName.errors?.['minlength']['actualLength']}}
        caratteri
      </div>
      <input
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid}"
        type="text" name="name"
        [ngModel]="active?.name"
        #inputName="ngModel"
        required minlength="5" placeholder="name"
      >
      <input
        class="form-control my-2"
        [ngClass]="{'is-invalid': inputCity.invalid && f.dirty, 'is-valid': inputCity.valid}"
        [ngModel]="active?.city"
        #inputCity="ngModel"
        type="text" name="city"  required placeholder="city *"
      >

      <select
        [ngModel]="active?.gender"  name="gender" class="form-control" required
        #inputGender="ngModel"
        [ngClass]="{'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid}"
      >
        <option [value]="null">Select gender</option>
        <option value="M">male</option>
        <option value="F">female</option>
      </select>

      <input type="checkbox" [ngModel]="active?.subscription" name="subscription" >

      <br>
      <button type="submit" [disabled]="f.invalid">
        {{active ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="resetForm(f)">CLEAR</button>
    </form>
  `,
})
export class UsersFormComponent {
  @Input() active: User | null = null;
  @Output() save = new EventEmitter<User>()
  @Output() reset = new EventEmitter()

  resetForm(form: NgForm) {
    this.reset.emit()
    form.reset();
  }

}
