import {Component} from '@angular/core';
import {User} from '../../model/user';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs';

const API = 'http://localhost:3000';

@Component({
  selector: 'app-users',
  template: `
    <div class="alert alert-danger" *ngIf="error">Errore</div>

    <app-card title="Form" [isOpen]="false">
      <app-users-form
        [active]="activeUser"
        (reset)="resetForm()"
        (save)="saveUser($event)"
      ></app-users-form>
    </app-card>
    
    <app-card [title]="users.length + ' users'">
      <app-skeleton [data]="users" >
        <app-users-list
          [users]="users"
          [active]="activeUser"
          (deleteUser)="deleteUser($event)"
          (selectUser)="selectUser($event)"
        ></app-users-list>
      </app-skeleton>
    </app-card>
  `,
})
export class UsersComponent {
  users: User[] = [];
  activeUser: User | null = null;
  error = false;

  constructor(private http: HttpClient) {
    http.get<User[]>('http://localhost:3000/users')
      .pipe(delay(2000))
      .subscribe(
        (res) => {
          this.users = res;
        },
        err => {
          this.error = true;
        }
      )
  }

  saveUser(user: User) {
    this.error = false;
    if (this.activeUser) {
      this.edit(user);
    } else {
      this.add(user)
    }
  }

  edit(user: User) {
    this.http.put<User>(`${API}/users/${this.activeUser?.id}`, user)
      .subscribe((user) => {
        const index = this.users.findIndex(u => u.id === this.activeUser?.id)
        this.users[index] = user;
        this.activeUser =  user
      })

  }

  add(user: User) {
    this.http.post<User>(`${API}/users`, user)
      .subscribe((res) => {
        this.users.push(res);
        // form.reset();
      })
  }

  deleteUser(userToDelete: User) {
    this.http.delete(`${API}/users/${userToDelete.id}`)
      .subscribe(() => {
        const index = this.users.findIndex(u => u.id === userToDelete.id)
        this.users.splice(index, 1)
        if (userToDelete.id === this.activeUser?.id) {
          this.activeUser = null;
        }
      })
  }

  resetForm() {
    this.activeUser = null;
  }

  selectUser(user: User) {
    this.activeUser = user;
  }
}
