import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page404',
  template: `
    <p>
      La pagina non esiste!
    </p>
    
    <button routerLink="/home">vai alla home</button>
  `,
})
export class Page404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
