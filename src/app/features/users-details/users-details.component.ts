import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'app-users-details',
  template: `
    <p>
      users-details works!
    </p>
    
    <h1>{{user?.name}}</h1>
    <h1 *ngIf="user">{{user.city}}</h1>
  `,
})
export class UsersDetailsComponent implements OnInit {
  user: User | null = null;

  constructor(
    private activatedRouted: ActivatedRoute,
    private http: HttpClient
  ) {
    const id = activatedRouted.snapshot.params['userId'];
    http.get<User>(`http://localhost:3000/users/${id}` )
      .subscribe(res => this.user = res)

  }

  ngOnInit(): void {
  }

}
