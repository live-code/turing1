import {ChangeDetectorRef, Component} from '@angular/core';
import {Address, User} from './model/user';

@Component({
  selector: 'app-root',
  template: `
    <div>{{user.name}}</div>
    <div *ngIf="user.age >= 18">Adult Only</div>
    <div [hidden]="user.age < 18">Adult only</div>
    <div [style.display]="user.age < 18 ? 'none' : null">Adult only</div>
    <div [class]="user.gender === 'M' ? 'male' : 'female' ">1. {{user.gender}}</div>
    <div
      [class.male]="user.gender === 'M'"
      [class.female]="user.gender === 'F'"
    >2. {{user.gender}}</div>

    <div [class]="getClsGender()">3. {{user.gender}}</div>

    <div
      [ngClass]="{
        'male': user.gender === 'M',
        'female': user.gender === 'F'
      }"
    >4. {{user.gender}}</div>

    <div
      [ngClass]="{
        'male': user.gender === 'M',
        'female': user.gender === 'F'
      }"
      [style.font-size.px]="fontSize"
    >
      5. {{user.gender}}
      <button (click)="fontSize = fontSize + 5">+</button>
    </div>

    <img width="200" [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + this.user.city + '&size=600,400'" alt="">

    <div>{{user.birthday | date: 'dd MMMM yy @ hh:mm'}}</div>
    <div>{{user.bitcoins | number: '1.2-2'}}</div>

    <pre>{{user | json}}</pre>
  `,
  styles: [`
    .male { background-color: blue }
    .female { background-color: pink }
  `]
})
export class AppComponentBackup {
  fontSize = 20;
  user: User = {
    id: 1,
    name: 'Fabio',
    age: 15,
    gender: 'F',
    city: 'Palermo',
    birthday: 1598910648000,
    bitcoins: 1
  }

  constructor() {
    setTimeout(() => {
      this.user.age = 30;
      this.user.gender = 'M';
      this.user.city = 'Trieste'
    }, 2000)
  }

  getClsGender() {
    switch (this.user.gender) {
      case 'M': return 'male'
      case 'F': return 'female'
      default: return 'male'
    }
  }

}


