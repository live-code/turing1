export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F';
  city: string;
  subscription: boolean;
  address?: Address
}

export interface Address {
  city: string;
  zip: string;
}
