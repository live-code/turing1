import { Component, OnInit } from '@angular/core';
import { ThemeService } from './services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div class="p-2" [ngClass]="{'bg-dark': themeService.theme === 'dark'}">
      <button routerLink="home">Home</button>
      <button routerLink="users">Users</button>
      <button routerLink="contacts">Contacts</button>
    </div>
    {{themeService.theme}}
    <hr>
  `,
})
export class NavbarComponent {
  constructor(public themeService: ThemeService) {}
}
