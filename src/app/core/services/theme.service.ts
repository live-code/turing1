import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _value;

  constructor() {
    this._value = localStorage.getItem('theme') || 'dark'
  }

  set theme(value: string) {
    localStorage.setItem('theme', value)
    this._value = value;
  }

  get theme(): string {
    return this._value;
  }
}
