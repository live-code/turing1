import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { HelloComponent } from './shared/hello.component';
import { HomeComponent } from './features/home/home.component';
import { UsersComponent } from './features/users/users.component';
import { Page404Component } from './features/page404/page404.component';
import { UsersDetailsComponent } from './features/users-details/users-details.component';
import { NavbarComponent } from './core/navbar.component';
import { MapquestComponent } from './shared/mapquest.component';
import { IconGenderComponent } from './shared/icon-gender.component';
import { CardComponent } from './shared/card.component';
import { TabBarComponent } from './shared/tab-bar.component';
import { UsersListComponent } from './features/users/components/users-list.component';
import { UsersFormComponent } from './features/users/components/users-form.component';
import { UsersListItemComponent } from './features/users/components/users-list-item.component';
import { SkeletonComponent } from './shared/skeleton.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    UsersComponent,
    HomeComponent,
    ContactsComponent,
    Page404Component,
    UsersDetailsComponent,
    NavbarComponent,
    MapquestComponent,
    IconGenderComponent,
    CardComponent,
    TabBarComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent,
    SkeletonComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'users', component: UsersComponent },
      { path: 'users/:userId', component: UsersDetailsComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '**', component: Page404Component},
    ]),
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
