import { Component, ViewEncapsulation } from '@angular/core';

const API = 'http://localhost:3000';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    
    <div class="container m-3">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
}
