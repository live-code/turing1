import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div 
        class="card-header" 
        [ngClass]="headerCls"
        (click)="toggleHandler()"
      >
        {{title}}
        <div class="pull-right" *ngIf="icon">
          <i (click)="iconClick.emit()" class="fa" [ngClass]="'fa-' + icon"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent  {
  @Input() title: string | undefined;
  @Input() headerCls: string = 'bg-dark text-white';
  @Input() icon: string = '';
  @Input() isOpen = true;
  @Output() iconClick = new EventEmitter()
  @Output() isOpenChange = new EventEmitter()

  toggleHandler() {
    this.isOpen = !this.isOpen;
    this.isOpenChange.emit(this.isOpen)
  }

}
