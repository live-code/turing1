import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-mapquest',
  template: `
    <img [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + city + '&size=600,400&zoom=' + zoom" width="100%">
  `
})
export class MapquestComponent {
  @Input() city: string = 'Trieste'
  @Input() zoom: number = 6;
}

