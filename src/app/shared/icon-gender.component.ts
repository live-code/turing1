import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon-gender',
  template: `
    <i
      class="fa "
      [ngClass]="{
        'fa-mars male': gender === 'M', 
        'fa-venus female': gender === 'F'
      }"
    ></i>
  `,
})
export class IconGenderComponent {
 @Input() gender: string | undefined;
}
