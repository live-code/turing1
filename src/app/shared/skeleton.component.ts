import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton',
  template: `
    <div *ngIf="!data.length" style="background: gray; border-radius: 20px">
      loading....
    </div>
    <div *ngIf="data.length">
      <ng-content></ng-content>
    </div>
  `,
})
export class SkeletonComponent {
  @Input() data: any;

}
